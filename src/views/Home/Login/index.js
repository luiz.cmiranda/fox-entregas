import { useFormik } from "formik";
import { Container, Row, Col, Form, Alert } from "react-bootstrap";
import { Button } from "../../../components/Button";
import { FormField } from "../../../components/FormField";
import { Layout } from "../../../components/Header/Layout";
import { PageTitle } from "../../../components/Header/PageTitle";
import * as yup from "yup";
import { loginUser } from "../../../services/loginUser";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import {useDispatch} from "react-redux"
import { updateUser } from "../../../store/slices/userSlice";
import { Link } from "react-router-dom";

export const LoginView = () => {
  const history = useHistory();
  const dispath = useDispatch()
  const [generalErrors, setGeneralErrors] = useState();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: yup.object().shape({
      email: yup
        .string()
        .required("Por favor, preencha o e-mail.")
        .email("Por favor preencha um e-mail válido."),
      password: yup.string().required("Preencha a senha."),
    }),
    onSubmit: async (values) => {
      setGeneralErrors(undefined);
      const { error, data } = await loginUser(values);
      if (!error) {
        dispath(updateUser(data))
        history.push("/novo-pedido");
        return;
      }
      const message =
        data.msg === "Email or password invalid."
          ? "Email ou senha inválidos."
          : "Ocorreu um erro no login. Por favor, Tente novamente.";
      setGeneralErrors(message);
    },
  });
  const getFieldProps = (fieldName) => {
    return {
      ...formik.getFieldProps(fieldName),
      error: formik.errors[fieldName],
      isInvalid: formik.touched[fieldName] && formik.errors[fieldName],
      isValid: formik.touched[fieldName] && !formik.errors[fieldName],
    };
  };
  return (
    <Layout>
      <Container>
        <Row className="justify-content-center">
          <Col lg={5} xl={4}>
            <PageTitle>Login</PageTitle>
            <Form onSubmit={formik.handleSubmit}>
              <FormField
                {...getFieldProps("email")}
                label="E-mail"
                placeholder="Preencha seu e-mail de acesso"
                type="email"
              />
              <FormField
                {...getFieldProps("password")}
                label="Senha"
                placeholder="Preencha sua senha de acesso"
                type="password"
              />
              {generalErrors && <Alert variant="danger">{generalErrors}</Alert>}
              <Button
                type="submit"
                block
                className="mt-3 mb-4"
                disabled={formik.isValidating || formik.isSubmitting}
                loading={formik.isValidating || formik.isSubmitting}
              >
                Entrar
              </Button>
              <p className="text-center">
                Não possui conta?
                <Link to="/cadastro" className="d-block">
                  Cadastrar
                </Link>
              </p>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};
