import React, { useEffect } from "react";
import styled from "styled-components";
import bgMobile from "../../assets/bg-fox-entregas-mobile.jpg";
import bgDesktop from "../../assets/bg-fox-entregas.jpg";
import Aos from "aos";
import "aos/dist/aos.css";
import { Container } from "react-bootstrap";
import { Layout } from "../../components/Header/Layout";
import { Button } from "../../components/Button";
import { selectUserLoggedIn } from "../../store/slices/userSlice";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export const HomeView = () => {
  useEffect(() => {
    Aos.init({});
  }, []);
  const isUserLogeedIn = useSelector(selectUserLoggedIn);
  return (
    <>
      <Layout startTransparent={true} withouMargin={true}>
        <WrapStyled className="vh-100">
          <Container
            className="vh-100 flex-column 
        d-flex justify-content-center "
          >
            <TitleStyled
              className="text-center text-lg-left text-white mt-auto mt-lg-0"
              data-aos="fade-up"
              data-aos-duration="3000"
            >
              Fazemos sua entrega de forma rápida e barata
            </TitleStyled>
            <div
              className="mt-auto mt-lg-3 d-flex 
            flex-column align-items-center align-items-lg-start mb-3">
              {isUserLogeedIn ? (
                <>
                  <Button
                    variant="success"
                    size="lg"
                    to="/novo-pedido" forwardedAs={Link}
                    className="mb-3"
                    data-aos="flip-left"
                    data-aos-easing="ease-out-cubic"
                    data-aos-duration="2000"
                  >
                    Novo Pedido
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    variant="success"
                    size="lg"
                    to="/cadastro" forwardedAs={Link}
                    className="mb-3"
                    data-aos="flip-left"
                    data-aos-easing="ease-out-cubic"
                    data-aos-duration="2000"
                  >
                    Criar conta
                  </Button>

                  <Button
                    variant="success"
                    size="lg"
                    to="/login" forwardedAs={Link}
                    data-aos="flip-left"
                    data-aos-easing="ease-out-cubic"
                    data-aos-duration="2000"
                  >
                    Fazer login
                  </Button>
                </>
              )}
            </div>
          </Container>
        </WrapStyled>
      </Layout>
    </>
  );
};

const WrapStyled = styled.div`
  background: url(${bgMobile}) no-repeat center center;
  background-size: cover;
  @media screen and (min-width: 576px) {
    background-image: url(${bgDesktop});
  }
  @media screen and (min-width: 768px) {
    background-image: url(${bgMobile});
  }
  @media screen and (min-width: 992px) {
    background-image: url(${bgDesktop});
  }
`;

const TitleStyled = styled.h1`
  font-size: 2.25rem;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  @media screen and (min-width: 992px) {
    font-size: 3rem;
    max-width: 500px;
  }
`;
