import { Container } from "react-bootstrap";
import { Layout } from "../../../components/Header/Layout";
import { PageTitle } from "../../../components/Header/PageTitle";

export const NotFoundView = () => {
  return (
    <Layout>
      <Container className="text-center">
        <PageTitle>Página não encontrada.</PageTitle>
        <p>A página que você está tentando acessar não foi encontrada ou foi movida.</p>
        <p>Utilize o menu superior para encontrar o que deseja.</p>
      </Container>
    </Layout>
  );
};
