import { Container, Row, Col } from "react-bootstrap";
import { Layout } from "../../components/Header/Layout"
import { Button} from "../../components/Button"
import {PageTitle} from "../../components/Header/PageTitle"
import { Link } from "react-router-dom";

export const NewOrderSuccessView = () => {
  return (
    <Layout>
      <Container className="text-center">
        <Row className="justify-content-center">
          <Col lg={5} xl={4}>
            <PageTitle>Pedido recebido com sucesso!</PageTitle>
            <p>
              Entraremos em contato pelo seu telefone e pelo e-mail com os
              detalhes do entregador que irá realizar sua entrega.
            </p>
            <Button 
            variant="success"
            to="/novo-pedido"
            forwardedAs={Link}
            size="lg"
            > 
            Fazer outro pedido</Button>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};
