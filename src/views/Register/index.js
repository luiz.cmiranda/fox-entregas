import { useFormik } from "formik";
import * as yup from "yup";
import { IMaskInput } from "react-imask";
import { Container, Form, Row, Col, Alert } from "react-bootstrap";
import { Button } from "../../components/Button";
import { FormField } from "../../components/FormField";
import { Layout } from "../../components/Header/Layout";
import { PageTitle } from "../../components/Header/PageTitle";
import { createUser } from "../../services/createUser";
import { useState } from "react";
import { getErrorDescription } from '../../services/getErrorDescription'
import { useHistory, Link } from 'react-router-dom'


export const RegisterView = () => {
  const [generalErrors, setGeneralErrors] = useState()
  const history = useHistory()
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      phone: '',
      password: '',
      agree: false
    },
    validationSchema: yup.object().shape({
      name: yup.string()
        .required('Por favor, preencha o nome.')
        .min(5, 'Informe pelo menos 5 caracteres.'),
      email: yup.string()
        .required('Por favor, preencha o e-mail.')
        .email('Por favor preencha um e-mail válido.'),
      phone: yup.string()
        .required('Por favor, preencha o telefone.'),
      password: yup.string()
        .required('Preencha a senha.')
        .min(8, 'Informe pelo menos 8 caractéres.')
        .max(50, 'Informe no máximo 50 caractéres.'),
      agree: yup.boolean()
        .equals([true], 'É preciso aceitar os termos.')
    }),
    onSubmit: async (values, { setFieldError }) => {
      setGeneralErrors(undefined)
      const { error, data } = await createUser(values)
      if (!error) {
        history.push('/login')
        return;
      }
      if (data.msg === 'ValidationError') {
        data.errors.forEach(errorDetail => {
          const message = getErrorDescription(errorDetail.msg)
          setFieldError(errorDetail.param, message)
        })
        return;
      }
      setGeneralErrors('Ocorreu um erro ao cadastrar. Por favor, Tente novamente.')
    }
  })
  const getFieldProps = fieldName => {
    return {
      ...formik.getFieldProps(fieldName),
      error: formik.errors[fieldName],
      isInvalid: formik.touched[fieldName] && formik.errors[fieldName],
      isValid: formik.touched[fieldName] && !formik.errors.name
    }
  }
  return (
    <Layout>
      <Container>
        <Row className='justify-content-center'>
          <Col lg={5} xl={4}>
            <PageTitle>Nova Conta</PageTitle>
            <Form onSubmit={formik.handleSubmit}>
              <FormField
                {...getFieldProps('name')}
                label='Nome'
                placeholder='Nome'
              />
              <FormField
                {...getFieldProps('email')}
                label='E-mail'
                placeholder='Ele será o seu usuário'
                type='email'
              />
              <FormField
                {...getFieldProps('phone')}
                label='Telefone'
                placeholder='(00) 00000-0000'
                as={IMaskInput}
                mask={[
                  { mask: '(00) 0000-0000' },
                  { mask: '(00) 00000-0000' }
                ]}
                onChange={undefined}
                onAccept={value => formik.setFieldValue('phone', value)}
              />
              <FormField
                {...getFieldProps('password')}
                label='Senha'
                placeholder='Informe sua senha de acesso'
                type='password'
              />
              <Form.Check
                {...formik.getFieldProps('agree')}
                type='checkbox'
                label={<span>Eu li e aceito os <a href='/termos-de-uso.pdf' target='_blank'>Termos de Uso</a>.</span>}
              />
              {formik.touched.agree && formik.errors.agree && (
                <Form.Control.Feedback type='invalid' className='d-block'>
                  {formik.errors.agree}
                </Form.Control.Feedback>
              )}
              {generalErrors && (
                <Alert variant='danger'>
                  {generalErrors}
                </Alert>
              )}
              <Button
                type='submit'
                block
                className='mt-3 mb-4'
                disabled={formik.isValidating || formik.isSubmitting}
                loading={formik.isValidating || formik.isSubmitting}
              >
                Criar conta
              </Button>
              <p className='text-center'>
                Já possui conta?
                <Link to='/login' className='d-block'>Entrar</Link>
              </p>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}
