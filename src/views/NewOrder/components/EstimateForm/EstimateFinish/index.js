import { Alert } from "bootstrap";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { PayPalButton } from "../../../../../components/PayPalButton";
import { createOrder } from "../../../../../services/createOrder";
import {
  clearCurrentEstimate,
  selectCurrentEstimate,
} from "../../../../../store/slices/estimateSlice";

export const EstimateFinish = () => {
  const currentEstimate = useSelector(selectCurrentEstimate);
  const dispatch = useDispatch();
  const history = useHistory();
  const [generalError, setGeneralError] = useState();
  const [loading, setLoading] = useState(false);
  const handleError = () => {
    setGeneralError(
      "Falha ao processar o pagamento. Entre em contato conosco."
    );
  };
  const handleSuccess = async (orderDetails) => {
    setGeneralError(undefined);
    setLoading(true);
    const { error } = await createOrder({
      estimateId: currentEstimate.id,
      gatewayId: orderDetails.id,
      comments: currentEstimate.comments,
    });
    setLoading(false);
    if (!error) {
      const action = clearCurrentEstimate();
      dispatch(action);
      history.push("/novo-pedido/sucesso");
      return;
    }
    setGeneralError("Falha ao pagar. Entre em contato conosco.");
  };
  return (
    <div className="mt-3">
      {generalError && <Alert variant="danger">{generalError}</Alert>}
      <PayPalButton
        value={currentEstimate.value}
        customId={currentEstimate.id}
        onSuccess={handleSuccess}
        onError={handleError}
        disabled={loading}
      />
      {loading && <p className="text-center">Finalizando pedido...</p>}
    </div>
  );
};
