import { useSelector } from "react-redux"
import styled from "styled-components"
import {selectCurrentEstimate} from "../../../../../store/slices/estimateSlice"
import { EstimateFinish } from "../EstimateFinish"
import {EstimateMap} from "../EstimateMap"
import { EstimateNumbers } from "../EstimateNumbers"

export const EstimateDetails = () => {
 const hasCurrentEstimate = useSelector(selectCurrentEstimate)
  if (! hasCurrentEstimate) {
    return (
        < WithoutEstiamteStyled className="d-none d-md-flex">
        <p className="m-0">Preencha os dados ao lado para ver o preço.</p>
        </ WithoutEstiamteStyled>
    )
  }  
  return <WithEstimateStyled>
      <EstimateMap/>
      <EstimateNumbers/>
      <EstimateFinish />
  </WithEstimateStyled>
}

const WithoutEstiamteStyled = styled.div`
background-color: #EFEFEF;
border: 1px dashed #000000;
display: flex;
justify-content: center;
align-items: center;
height: 100%;
padding: 0 100px;
text-align: center;

`
const WithEstimateStyled = styled.div`
height:100%;
display: flex;
flex-direction: column;

`