import { Col, Container, Row } from "react-bootstrap";
import { Layout } from "../../components/Header/Layout";
import { PageTitle } from "../../components/Header/PageTitle";
import { EstimateForm } from "./components/EstimateForm";
import { EstimateDetails } from "./components/EstimateForm/EstimateDetails";

export const NewOrderView = () => {
  return (
    <Layout>
      <Container>
        <PageTitle>Novo Pedido</PageTitle>
        <Row>
          <Col xs={12} md={6} lg={7}>
            <EstimateForm/>
          </Col>  
          <Col xs={12} md={6} lg={5}>
           <EstimateDetails/>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};
