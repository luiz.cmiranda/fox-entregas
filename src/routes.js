import { BrowserRouter, Switch, Route } from "react-router-dom";

import { HomeView } from "./views/Home";
import { LoginView } from "./views/Home/Login";
import { NotFoundView } from "./views/Home/NotFound";
import { NewOrderView } from "./views/NewOrder";
import { RegisterView } from "./views/Register";
import { PrivateRoute } from "./components/PrivateRoute";
import { PublicRouterOnly } from "./components/PublicRouteOnly";
import { NewOrderSuccessView } from "./views/NewOrderSuccess";

export const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact>
          <HomeView />
        </Route>
        <PublicRouterOnly path="/cadastro" exact>
          <RegisterView />
        </PublicRouterOnly>
        <Route path="/login" exact>
          <LoginView />
        </Route>
        <PrivateRoute path="/novo-pedido" exact>
          <NewOrderView />
        </PrivateRoute>
        <PrivateRoute path="/novo-pedido/sucesso"
         exact> 
         <NewOrderSuccessView/> 
         </PrivateRoute>
        <Route path="*">
          <NotFoundView />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};
