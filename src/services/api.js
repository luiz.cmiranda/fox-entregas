import axios from "axios";
import { updateUser } from "../store/slices/userSlice";
import { getStorageItem, setStorageItem } from "./storage";

const user = JSON.parse(getStorageItem("user"));
export const api = axios.create({
  baseURL: "https://southamerica-east1-foxentregas.cloudfunctions.net/api",
  headers: {
    owner: "lecojessica",
    Authorization: user ? `Bearer ${user.accessToken}` : undefined,
  },
});

export const setupInterceptor = (store) => {
  api.interceptors.response.use(
    (response) => response,
    async (error) => {
      const originalConfig = error.config;
      if (
        error.response.status === 401 &&
        error.response.data.msg === "Access token expired."
      ) {

        try {
        const user = JSON.parse(getStorageItem("user"));
        const response = await api.post("/refresh-token", {
          token: user.refreshToken,
        });
        setStorageItem("user", JSON.stringify(response.data));
        api.defaults.headers[
          "Authorization"
        ] = `Bearer ${response.data.accessToken}`;
        store.dispatch(updateUser(response.data));
        originalConfig.headers[
          "Authorization"
        ] = `Bearer ${response.data.accessToken}`;
        return api(error.config);
      } catch (err) {
        return Promise.reject(err)
      }
     }
     return Promise.reject(error)
    }
  )
}
