import { api } from "./api"

export const createOrder = async (orderData) => {
  try {
    const response = await api.post('/orders', orderData)
    return {
      error: false,
      data: response.data
    }
  } catch (error) {
    return {
      error: true,
      data: error.response?.data || 'Erro inesperado.'
    }
  }
}
