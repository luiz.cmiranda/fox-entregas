import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./slices/userSlice";
import estimateReducer from "./slices/estimateSlice";

export default configureStore({
  reducer: {
    user: userReducer,
    estimate: estimateReducer,
  },
});
