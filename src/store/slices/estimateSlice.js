import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  currentEstimate: null,
};

export const slice = createSlice({
  name: "estimate",
  initialState,
  reducers: {
    setCurrentEstimate: (state, action) => {
      state.currentEstimate = action.payload;
    },
    clearCurrentEstimate: (state, action) => {
      state.currentEstimate = null;
    },
  },
});
export const { setCurrentEstimate, clearCurrentEstimate } = slice.actions;

export default slice.reducer;

export const selectHasCurrenteEstimate = (state) => !!state.estimate.currentEstimate;

export const selectCurrentEstimate = state => state.estimate.currentEstimate; 
