import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import { setupInterceptor } from "./services/api";
import store from "./store/store";
import {PayPalScriptProvider} from "@paypal/react-paypal-js"

const payPalOptions = {
  "client-id": "AfpEADIAO7OjFa55R-Jlt8uWTj8ZYrH889-izkqNVhX9vuTQLu16TUu3ud66g8nbRZpaNpg_-r-pG5gV",
   currency: "BRL"

}

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PayPalScriptProvider options={payPalOptions}>
        <App />
      </PayPalScriptProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

setupInterceptor(store);
