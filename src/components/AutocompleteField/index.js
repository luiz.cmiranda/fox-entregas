
import { Autocomplete } from "@react-google-maps/api";
import { useRef } from "react";
import { FormField } from "../FormField";
import { LoadGoogleScript } from "../LoadGoogleScript";

export const AutocompleteField = ({ value, onChange, ...fieldProps }) => {
  const autocompleteRef = useRef(null);
  const handleLoad = (autocomplete) => {
    autocompleteRef.current = autocomplete;
  };
  const handleChange = () => {
    const place = autocompleteRef.current.getPlace();
    if (place.formatted_address) {
      const address = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
        address: place.formatted_address,
      };
      onChange(address);
    }
  };
  return (
    <LoadGoogleScript>
      <Autocomplete 
      onLoad={handleLoad} 
      onPlaceChanged={handleChange}
      restrictions={{
        country: "br"
      }}
      >
        <FormField {...fieldProps} 
        defaultValue={value.address || ''}
        />
      </Autocomplete>
    </LoadGoogleScript>
  );
};
