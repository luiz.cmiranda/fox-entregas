import { useSelector } from "react-redux"
import { Redirect, Route } from "react-router-dom"
import { selectUserLoggedIn } from "../../store/slices/userSlice"

export const PublicRouterOnly = ({children, ...otherProps}) => {
    const isUserLoggedIn = useSelector(selectUserLoggedIn)
    return (
        <Route 
        {...otherProps}
        render={() => !isUserLoggedIn ? children : <Redirect to={{pathName: "/novo-pedido"}}/> }
        />
    )
}