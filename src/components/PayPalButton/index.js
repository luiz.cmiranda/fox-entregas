import {PayPalButtons} from "@paypal/react-paypal-js"

export const PayPalButton = ({value, custom_id, onSuccess, onError, disabled}) => {
  return (
      <PayPalButtons
       createOrder={(data, actions) => {
          return actions.order.create({
             intent: "CAPTURE",
             purchase_units: [{
               amount: {
                  currency_code: "BRL",
                  value
               },
               custom_id: custom_id 
             }],
             application_context: {
                 brand_name: "Fox Entregas",
                 shipping_preference: "NO_SHIPPING"
             }
          })
       }}
       onApprove={(data, actions) => {
         return actions.order.capture().then(details => {
           onSuccess(details)
         })
       }}
       onError={onError}
       style={{}}
       disabled={disabled}
      />
  )
}