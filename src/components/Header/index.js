import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Navbar, Nav } from "react-bootstrap";

import Logo from "../../assets/logo-fox-entregas.svg";
import LogoBlue from "../../assets/logo-fox-entregas-azul.svg";
import { Button } from "../Button";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import Aos from "aos";
import "aos/dist/aos.css";
import { logoutUser } from "../../services/logoutUser";
import { deleteUser,selectUserLoggedIn } from "../../store/slices/userSlice";
import { Link, useHistory } from "react-router-dom";




export const Header = ({ startTransparent = false }) => {
  useEffect(() => {
    Aos.init({});
  }, []);

  const isUserLoggedIn = useSelector(selectUserLoggedIn);
  const dispatch = useDispatch()
  const history = useHistory()
  const [isTransparent, setIsTransparent] = useState(startTransparent);
  useEffect(() => {
    if (startTransparent) {
      const scrollChange = () => {
        const isLowScroll = window.scrollY < 50;
        setIsTransparent(isLowScroll);
      };
      window.addEventListener("scroll", scrollChange);
      return () => {
        window.removeEventListener("scroll", scrollChange);
      };
    }
  }, [isTransparent, startTransparent]);

  const handlelogout = () => {
    logoutUser()
    dispatch(deleteUser())
    history.push("/login")
  }

  return (
    <NavbarStyled
      fixed="top"
      bg={isTransparent ? undefined : "white"}
      expand="lg"
    >
      <Container>
        <Navbar.Brand to="/" as={Link}>
          <ImageStyled
            data-aos="fade-right"
            data-aos-offset="300"
            data-aos-easing="ease-in-sine"
            src={isTransparent ? Logo : LogoBlue}
            alt="Logo Fox Entregas"
            widht={194}
            height={51}
          />
        </Navbar.Brand>
        <ToggleStyeld>
          <FontAwesomeIcon
            icon={faBars}
            size="lg"
            className={isTransparent ? "text-white" : "text-dark"}
          />
        </ToggleStyeld>
        <CollapseStyled className="justify-content-center text-center justify-content-lg-end">
          <Nav>
            <LinkStyled isTransparent={isTransparent} to="/" forwardedAs={Link}>
              Início
            </LinkStyled>
            {isUserLoggedIn ? (
              <>
                <Button
                  className="mt-2 mt-lg-0 ml-lg-4"
                  to="/novo-pedido" forwardedAs={Link}
                  data-aos="fade-down">
                  Fazer pedido
                </Button>
                <Button
                  className="mt-2 mt-lg-0 ml-lg-4"
                  onClick={handlelogout}
                  data-aos="fade-down">
                  Sair
                </Button>
              </>
            ) : (
              <>
                <Button
                  className="mt-2 mt-lg-0 ml-lg-4"
                  to="/cadastro" forwardedAs={Link}
                  data-aos="fade-down"
                >
                  Criar conta
                </Button>
                <Button
                  className="mt-2 mt-lg-0 ml-lg-4"
                  to="/login" forwardedAs={Link}
                  data-aos="fade-down"
                >
                  Fazer login
                </Button>
              </>
            )}
          </Nav>
        </CollapseStyled>
      </Container>
    </NavbarStyled>
  );
};

const NavbarStyled = styled(Navbar)`
  transition: all 0.3s linear;
  ${(props) =>
    props.bg === "white" &&
    `
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  `}
`;

const ImageStyled = styled.img`
  @media screen and (min-width: 992px) {
    width: 266px;
    height: auto;
  }
`;

const ToggleStyeld = styled(Navbar.Toggle)`
  border: none;
`;

const CollapseStyled = styled(Navbar.Collapse)`
  @media screen and (max-width: 991px) {
    background: #fff;
    margin: 0 -1rem;
    padding: 1rem 2rem;
  }
`;

const LinkStyled = styled(Nav.Link)`
  color: #343a40 !important;
  @media screen and (min-width: 992px) {
    color: ${(props) => (props.isTransparent ? "#FFF" : "#343a40")} !important;
  }
`;
