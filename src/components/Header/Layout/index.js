import { Header } from "../index";
import { Footer } from "../Footer/index";
import styled from "styled-components";

export const Layout = ({ children, startTransparent, withouMargin }) => {
  return (
    <>
      <Header startTransparent={startTransparent} />
      <WrapStyled startTransparent={startTransparent}>{children}</WrapStyled>
      <Footer withouMargin={withouMargin} />
    </>
  );
};

const WrapStyled = styled.div`
  ${(props) =>
    !props.startTransparent &&
    `
  padding-top: 77px;
  @media screen and (min-width: 992px) {
  padding-top: 96px;
  }
`}
`;
