import { Container, Nav } from "react-bootstrap";
import { useSelector } from "react-redux";
import Logo from "../../../assets/logo-fox-entregas.svg";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookSquare,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";
import { selectUserLoggedIn } from "../../../store/slices/userSlice";
import { Link } from "react-router-dom";

export const Footer = ({ withouMargin = false }) => {
  const isUserLogeedIn = useSelector(selectUserLoggedIn);
  return (
    <FooterStyled
      className={["text-center", withouMargin ? "" : "mt-5"].join(" ")}
    >
      <Container className="d-lg-flex align-items-center">
        <Link to="/">
          <img src={Logo} alt="Logo Fox Entregas" width={255} height={67} />
        </Link>
        <Nav
          className="flex-column flex-lg-row my-4 
         my-lg-0 ml-lg-auto"
        >
          <LinkStyled to="/" forwardedAs={Link}> Início </LinkStyled>
          {isUserLogeedIn ? (
            <>
              <LinkStyled to="/novo-pedido" forwardedAs={Link}>Novo Pedido</LinkStyled>
            </>
          ) : (
            <>
              <LinkStyled to="/cadastro" forwardedAs={Link}> Cadastro</LinkStyled>
              <LinkStyled to="/login" forwardedAs={Link}> Login </LinkStyled>
            </>
          )}

          <LinkStyled href="/termos-de-uso.pdf" target="_blank">
            {" "}
            Termos de Uso{" "}
          </LinkStyled>
        </Nav>
        <Nav className="justify-content-center">
          <LinkStyled
            href="https://facebook.com"
            target="_blank"
            rel="noreferrer noopener"
            className="px-2"
          >
            <IconStyled icon={faFacebookSquare} />
          </LinkStyled>
          <LinkStyled
            href="https://instagram.com"
            target="_blank"
            rel="noreferrer noopener"
            className="px-2"
          >
            <IconStyled icon={faInstagram} />
          </LinkStyled>
        </Nav>
      </Container>
    </FooterStyled>
  );
};

const FooterStyled = styled.footer`
  background-color: #414141;
  color: #fff;
  padding: 30px 0 40px 0;
  @media screen and(min-width: 992px) {
    padding: 15px 0;
  }
`;

const LinkStyled = styled(Nav.Link)`
  color: #fff !important;
  &:hover {
    color: #ccc !important;
  }
`;

const IconStyled = styled(FontAwesomeIcon)`
  font-size: 40px;
`;
