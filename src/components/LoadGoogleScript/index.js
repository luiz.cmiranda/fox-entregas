import { useLoadScript } from "@react-google-maps/api";
import { Alert, Spinner } from "react-bootstrap";

const libraries = ["places"];

export const LoadGoogleScript = ({ children }) => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: "AIzaSyCgX9NvUd8qMdQEbjJ22DOqjGDKGPIIwxk",
    libraries,
  });
  if (loadError) {
    return (
      <Alert variant="danger">
        Falha ao carregar o Google. Recarregue a página.
      </Alert>
    );
  }
  if (!isLoaded) {
    return (
      <Spinner animation="grow" role="status">
        <span className="sr-only">Carregando...</span>
      </Spinner>
    );
  }
  return children;
};
