import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import {Routes} from "../src/routes"


function App() {
  return <Routes />;
}

export default App;
